// libvesc.h
//
// public interface for libvesc
#ifndef LIBVESC_H_
#define LIBVESC_H_

#include <stdint.h> // for int8_t, int32_t, etc.
#include <stdio.h> // for FILE, etc.

typedef enum vesc_packet_id {
    COMM_FW_VERSION = 0,
    COMM_JUMP_TO_BOOTLOADER,
    COMM_ERASE_NEW_APP,
    COMM_WRITE_NEW_APP_DATA,
    COMM_GET_VALUES,
    COMM_SET_DUTY,
    COMM_SET_CURRENT,
    COMM_SET_CURRENT_BRAKE,
    COMM_SET_RPM,
    COMM_SET_POS,
    COMM_SET_HANDBRAKE,
    COMM_SET_DETECT,
    COMM_SET_SERVO_POS,
    COMM_SET_MCCONF,
    COMM_GET_MCCONF,
    COMM_GET_MCCONF_DEFAULT,
    COMM_SET_APPCONF,
    COMM_GET_APPCONF,
    COMM_GET_APPCONF_DEFAULT,
    COMM_SAMPLE_PRINT,
    COMM_TERMINAL_CMD,
    COMM_PRINT,
    COMM_ROTOR_POSITION,
    COMM_EXPERIMENT_SAMPLE,
    COMM_DETECT_MOTOR_PARAM,
    COMM_DETECT_MOTOR_R_L,
    COMM_DETECT_MOTOR_FLUX_LINKAGE,
    COMM_DETECT_ENCODER,
    COMM_DETECT_HALL_FOC,
    COMM_REBOOT,
    COMM_ALIVE,
    COMM_GET_DECODED_PPM,
    COMM_GET_DECODED_ADC,
    COMM_GET_DECODED_CHUK,
    COMM_FORWARD_CAN,
    COMM_SET_CHUCK_DATA,
    COMM_CUSTOM_APP_DATA,
    COMM_NRF_START_PAIRING,
    COMM_GPD_SET_FSW,
    COMM_GPD_BUFFER_NOTIFY,
    COMM_GPD_BUFFER_SIZE_LEFT,
    COMM_GPD_FILL_BUFFER,
    COMM_GPD_OUTPUT_SAMPLE,
    COMM_GPD_SET_MODE,
    COMM_GPD_FILL_BUFFER_INT8,
    COMM_GPD_FILL_BUFFER_INT16,
    COMM_GPD_SET_BUFFER_INT_SCALE,
    COMM_GET_VALUES_SETUP,
    COMM_SET_MCCONF_TEMP,
    COMM_SET_MCCONF_TEMP_SETUP,
    COMM_GET_VALUES_SELECTIVE,
    COMM_GET_VALUES_SETUP_SELECTIVE,
    COMM_EXT_NRF_PRESENT,
    COMM_EXT_NRF_ESB_SET_CH_ADDR,
    COMM_EXT_NRF_ESB_SEND_DATA,
    COMM_EXT_NRF_ESB_RX_DATA,
    COMM_EXT_NRF_SET_ENABLED,
    COMM_DETECT_MOTOR_FLUX_LINKAGE_OPENLOOP,
    COMM_DETECT_APPLY_ALL_FOC,
    COMM_JUMP_TO_BOOTLOADER_ALL_CAN,
    COMM_ERASE_NEW_APP_ALL_CAN,
    COMM_WRITE_NEW_APP_DATA_ALL_CAN,
    COMM_PING_CAN,
    COMM_APP_DISABLE_OUTPUT,
    COMM_TERMINAL_CMD_SYNC,
    COMM_GET_IMU_DATA,
    COMM_BM_CONNECT,
    COMM_BM_ERASE_FLASH_ALL,
    COMM_BM_WRITE_FLASH,
    COMM_BM_REBOOT,
    COMM_BM_DISCONNECT
} vesc_packet_id;

typedef enum _vesc_fault_code {
    FAULT_CODE_NONE = 0,
    FAULT_CODE_OVER_VOLTAGE,
    FAULT_CODE_UNDER_VOLTAGE,
    FAULT_CODE_DRV8302,
    FAULT_CODE_ABS_OVER_CURRENT,
    FAULT_CODE_OVER_TEMP_FET,
    FAULT_CODE_OVER_TEMP_MOTOR
} _vesc_fault_code;

typedef struct lvesc_status {
    uint8_t controller_id;
    int8_t fault;
    float input_voltage;
    float input_current;
    float duty_cycle;
    float motor_speed;
    float motor_current;
    float motor_temperature;
    float mosfet_temperature;
    float id;
    float iq;
    float charge;
    float charge_regenerated;
    float energy;
    float energy_regenerated;
    int32_t tachometer;
    int32_t tachometer_absolute;
    float pid_position;
    float ntc_temp_mos[3];
} lvesc_status;


size_t vesc_write( vesc_packet_id id, const void * payload,
        size_t length, FILE * dev );
int vesc_read_packet( void * payload, size_t n, FILE * dev );

// Getters
void vesc_poll_firmware_version( FILE * dev );
// int vesc_read_firmware_version( FILE * dev );
void vesc_poll_status( FILE * dev );

// Setters
void vesc_set_duty_cycle( FILE * dev, const float duty_cycle );
void vesc_set_current( FILE * dev, const float current );
void vesc_set_rpm( FILE * dev, const float rpm );

// Other public methods
void vesc_stop( FILE * dev ) {
    vesc_set_duty_cycle( dev, 0.0 );
}
void vesc_fprintf_status( FILE * f, lvesc_status * status );
void vesc_printf_status( lvesc_status * status ) {
    vesc_fprintf_status( stdout, status );
}

#endif /* LIBVESC_H_ */
