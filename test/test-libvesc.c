#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "libvesc.h"


static void printbuf( const void * buffer, size_t length,
        const char * pfx, const char * sfx );

static void printbuf( const void * buffer, size_t length,
        const char * pfx, const char * sfx ){
    printf( pfx, NULL );
    for(int i = 0; i < length; i++ ){
        printf( "%02hhX ", ((char *)buffer)[i] );
    }
    printf( sfx, NULL );
}

// get vesc values
//   \x02\x01\x04@\x84\x03

static int test_vesc_get_status_crc_inclusive( void ) {
    const char data[3] = { 0x04, 0x40, 0x84 };
    printbuf( data, 3, "data with crc: ", "\n" );
    uint16_t crc = crc16xmodem( data, 1 );
    printbuf( &crc, 2, "crc: ", "\n" );
    uint16_t inclusive_crc = crc16xmodem( data, 3 );
    printbuf( &inclusive_crc, 2, "inclusive crc: ", "\n" );
    assert ( inclusive_crc == 0 );
    return EXIT_SUCCESS;
}


static int test_vesc_write_get_status( void ) {
    const int ne = 6;
    const unsigned char e[6] = { 0x02, 0x01, 0x04, 0x40, 0x84, 0x03 };
    unsigned char b[6] = { 0 };
    FILE * fp;

    fp = fopen( ".test.vesc_write_get_status", "wb" );
    if( NULL == fp ) {
        perror( "fopen()" );
        exit( EXIT_FAILURE );
    }
    size_t nw = vesc_write( COMM_GET_VALUES, NULL, 0, fp );
    fclose( fp );
    assert( nw == 6 );

    fp = fopen( ".test.vesc_write_get_status", "rb" );
    if( NULL == fp ) {
        perror( "fopen()" );
        exit( EXIT_FAILURE );
    }
    fread( b, 1, ne, fp );
    fclose( fp );

    printbuf( e, ne, "expected: ", "\n" );
    printbuf( b, ne, "observed: ", "\n" );

    assert( 0 == memcmp( b, e, ne ) );
    return EXIT_SUCCESS;
}


static int test_vesc_poll_status( void ) {
    const int ne = 6;
    const unsigned char e[6] = { 0x02, 0x01, 0x04, 0x40, 0x84, 0x03 };
    unsigned char b[6] = { 0 };
    FILE * fp;

    fp = fopen( ".test.vesc_poll_status", "wb" );
    if( NULL == fp ) {
        perror( "fopen()" );
        exit( EXIT_FAILURE );
    }
    vesc_poll_status( fp );
    fclose( fp );

    fp = fopen( ".test.vesc_poll_status", "rb" );
    if( NULL == fp ) {
        perror( "fopen()" );
        exit( EXIT_FAILURE );
    }
    fread( b, 1, ne, fp );
    fclose( fp );

    printbuf( e, ne, "expected: ", "\n" );
    printbuf( b, ne, "observed: ", "\n" );

    assert( 0 == memcmp( b, e, ne ) );
    return EXIT_SUCCESS;
}


static int test_vesc_set_duty_cycle( const float dc ) {
    FILE * fp = fopen( ".test.vesc_set_duty_cycle", "wb" );
    if( NULL == fp ) {
        perror( "fopen()" );
        exit( EXIT_FAILURE );
    }
    vesc_set_duty_cycle( fp, dc );
    fclose( fp );

    if( dc == 0.3 ) {
        const unsigned char e[10] = { 0x02, 0x05, 0x05, 0xE0, 0x93, 0x04,
            0x00, 0xB9, 0xEC, 0x03 };
        unsigned char b[10] = { 0 };

        fp = fopen( ".test.vesc_poll_status", "rb" );

        if( NULL == fp ) {
            perror( "fopen()" );
            exit( EXIT_FAILURE );
        }
        fread( b, 1, 10, fp );
        fclose( fp );

        printbuf( e, 10, "expected: ", "\n" );
        printbuf( b, 10, "observed: ", "\n" );

        assert( 0 == memcmp( b, e, 10 ) );
    }
    return EXIT_SUCCESS;
}

/*
static int test_vesc_printf_status( void ) {
    lvesc_status status = { 0 };

    FILE * fp = fopen( ".test.libvesc-fopen-target", "w+" );
    if( NULL == fp ) {
        perror( "fopen()" );
        exit( EXIT_FAILURE );
    }

    vesc_fake_status( fp, &status );
    vesc_printf_status( &status );
    return EXIT_SUCCESS;
}
*/

// set duty cycle to 30%
//   \x02\x05\x05\x00\x00u0\xe2\xa8\x03


int main ( void ) {
    int r = EXIT_SUCCESS;

    r &= test_vesc_get_status_crc_inclusive();
    r &= test_vesc_write_get_status();
    r &= test_vesc_poll_status();
    r &= test_vesc_set_duty_cycle( 0.3 );

    exit( r );
}
