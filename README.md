libvesc
=======

A library for communicating with [VESC] brushless motor electronic speed
controllers.

Adapted from the [blog post] and [example project][buc] by Benjamin Vedder.


[VESC]: https://vesc-project.com/
[blog post]: http://vedder.se/2015/10/communicating-with-the-vesc-using-uart/
[buc]: https://github.com/vedderb/bldc_uart_comm_stm32f4_discovery
_____________

Copyright 2018 M Jordan Stanway <m.j.stanway@alum.mit.edu>

Released under GPLv3, see LICENSE.md
