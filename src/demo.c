#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <poll.h>

#include "libvesc.h"

#define TIMEOUT_NS 500000000L

int main( int argc, char* argv[] ){
    char device[16] = "/dev/ttyUSB0";
    float duty_cycle = 0.0;
    int repeat = 3;
    lvesc_status status = { 0 };

    if( ( argc < 2 ) || ( argc >= 5 ) ) {
        fprintf( stderr, "Usage: %s [device] [duty-cycle] [timeout]\n",
                argv[0] );
        exit( EXIT_FAILURE );
    }
    if( argc >= 2 ) strcpy( device, argv[1] );
    if( argc >= 3 ) duty_cycle = atof( argv[2] );
    if( argc >= 4 ) repeat = atoi( argv[3] );

    struct termios opts = { 0 };
    opts.c_iflag = IGNBRK;
    opts.c_oflag = 0;
    opts.c_cflag = B57600 | CS8 | CLOCAL | CREAD;
    opts.c_lflag = 0;
    opts.c_cc[VMIN] = 1;
    opts.c_cc[VTIME] = 0;

    FILE * sio = fopen( device, "wb+" );
    if( NULL == sio ) {
        perror( "fopen()" );
        fprintf( stderr, "could not open device %s\n", device );
        exit( EXIT_FAILURE );
    }
    if( -1 == tcsetattr( fileno( sio ), TCSAFLUSH, &opts ) ) {
        perror( "tcsetattr()" );
        fputs( "trouble setting termios attributes", stderr );
        exit( EXIT_FAILURE );
    }

    struct timespec timeout = { .tv_sec = 0, .tv_nsec = TIMEOUT_NS };
    struct pollfd pfd = { .fd = fileno( sio ), .events = POLLIN };

    for( int n = 0; n < repeat; n++ ) {
        vesc_set_duty_cycle( sio, duty_cycle );
        vesc_poll_status( sio );
        switch( ppoll( &pfd, 1, &timeout, NULL ) ) {
            case -1:
                perror( "poll" );
                exit( EXIT_FAILURE );
            case 0:
                fprintf( stderr, "poll timed out\n" );
                break;
            case 1:
                assert( pfd.revents & POLLIN );
                if( pfd.revents & POLLIN ) {
                    vesc_read_packet( &status, sizeof( status ), sio );
                    vesc_printf_status( &status );
                } else {
                    fprintf( stderr, "\tnot sure how this would happen\n" );
                }
                nanosleep( &timeout, NULL );
        }
    }

    fclose( sio );
    exit( EXIT_SUCCESS );
}
