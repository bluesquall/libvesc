// libvesc.c

#include <assert.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <arpa/inet.h>

#include <libcrcrc.h>

#include "libvesc.h"

#define STX 0x02
#define ETX 0x03
#define VESC_PACKET_SHORT_MAX 256
#define VESC_STATUS_PAYLOAD_LENGTH 65

#if defined NDEBUG
#define R2_DEBUG 0
#else
#define R2_DEBUG 1
#endif

#define r2_debug_printf( fmt, ... ) \
    do { if( R2_DEBUG ) fprintf ( stderr, fmt, __VA_ARGS__ ); } while( 0 )
//TODO move to r2 header library

// low-level, internal methods -- not exposed outside this library
static int32_t hftoni32( const float hf, const float scale );
static float ni32tohf( const int32_t ni, const float scale );
static int16_t hftoni16( const float hf, const float scale );
static float ni16tohf( const int16_t ni, const float scale );
static size_t vread( char * b, size_t length, uint16_t * crc, FILE * dev );
static size_t vreadu16( uint16_t * u, uint16_t * crc, FILE * dev );
static size_t vreadi32( int32_t * i, uint16_t * crc, FILE * dev );
static size_t vreadf16( float * f, const float scale, uint16_t * crc, FILE * dev );
static size_t vreadf32( float * f, const float scale, uint16_t * crc, FILE * dev );
static size_t vread_header( FILE * dev );
static size_t vread_fault( lvesc_status * s, uint16_t * crc, FILE * dev );
static int vread_status( lvesc_status * s, size_t n, uint16_t * crc, 
        FILE * dev );

/* convert a float on the host to an integer in network byte order
 *
 * Use explicit casting at each step to encourage the expected behavior.
 * Compilers can optimize steps out as appropriate.
 */
static int32_t hftoni32( const float hf, const float scale ) {
    int32_t hi = (int32_t)( hf * scale );
    uint32_t nu = htonl( (uint32_t)hi );
    int32_t ni = (int32_t)nu;
    return ni;
}


static float ni32tohf( const int32_t ni, const float scale ){
    uint32_t hu = ntohl( (uint32_t)ni );
    int32_t hi = (int32_t)hu;
    float hf = hi / scale;
    return hf;
}


static int16_t hftoni16( const float hf, const float scale ) {
    int16_t hi = (int16_t)( hf * scale );
    uint16_t nu = htons( (uint16_t)hi );
    int16_t ni = (int16_t)nu;
    return ni;
}


static float ni16tohf( int16_t ni, const float scale ){
    r2_debug_printf( "ni16tohf, ni: %" PRId16 "\n", ni );
    uint16_t nu = (uint16_t)ni;
    r2_debug_printf( "ni16tohf, nu: %" PRIu16 " 0x%" PRIx16 "\n", nu, nu );
    uint16_t hu = ntohs( nu );
    r2_debug_printf( "ni16tohf, hu: %" PRIu16 " 0x%" PRIx16 "\n", hu, hu );
    int16_t hi = (int16_t)hu;
    r2_debug_printf( "ni16tohf, hi: %" PRId16 " 0x%" PRIx16 "\n", hi, hi );
    float hf = hi / scale;
    r2_debug_printf( "ni16tohf, hf: %f 0x%" PRIx32 "\n", hf, (int32_t)hf );
    return hf;
}


size_t vesc_write( vesc_packet_id id, const void * payload,
        size_t length,  FILE * dev ) {
    uint16_t crc = 0;
    size_t n = 4;
    if( length < VESC_PACKET_SHORT_MAX - 1 ) {
        fputc( STX, dev );
        fputc( (uint8_t)( length + 1 ), dev );
    } else {
        fputc( ETX, dev );
        uint16_t length_with_id = htons( (uint16_t)( length + 1 ) );
        fwrite( &length_with_id, 1, 2, dev );
        n += 1;
    }
    fputc( id, dev );
    n += fwrite( payload, 1, length, dev );
    crc = crc16xmodem_byte( 0, &id, 1 );
    if( length > 0 ) {
        crc = crc16xmodem_byte( crc, payload, length );
    }
    crc = htons( crc ); // write in network byte order
    n += fwrite( &crc, 1, 2, dev );
    fputc( ETX, dev );
    fflush( dev );
    assert( n == length + (length < VESC_PACKET_SHORT_MAX - 1 ? 6: 7 ) );
    return n;
}


void vesc_poll_firmware_version( FILE * dev ) {
    fprintf( stderr, "vesc_poll_firmware_version() not implemented" );
}


void vesc_poll_status( FILE * dev ) {
    size_t n = vesc_write( COMM_GET_VALUES, NULL, 0, dev );
    // n.b. since this is a static message, we could hard-code hdr & crc:
    // const char p[6] = { 0x02, 0x01, 0x04, 0x40, 0x84, 0x03 };
    // size_t n = fwrite( &p, 1, 6, dev );
    assert( n == 6 );
}


size_t vesc_read_into( char * b, size_t length, FILE * dev ) {
    /* TODO XXX
    if( reported_length + 2 > length ) {
        fprintf( stderr,
                "expected: %zu, reported: %zu, insufficient buffer\n",
                length, reported_length + 2 );
        // fflush( dev );
        return 0;
    } else {
        length = reported_length + 2;
    }
    */

    // TODO: write a method to change VMIN & VTIME as part of the read, if needed

    size_t length_read = fread( b, 1, length, dev );
    r2_debug_printf("tried to read %zu bytes, got %zu\n",
            length, length_read );

    if( length_read != length ){
        fprintf( stderr, "read %zu, expected %zu\n", length_read, length );
        // fflush( dev );
        return 0;
    } else if( crc16xmodem( b, length ) != 0 ){
        fprintf( stderr, "checksum mismatch\n" );
        for( int j = 0; j < length; j++ ) {
            fprintf( stderr, " %02hhX", b[j] );
        }
        fprintf( stderr, "\n" );
        // fflush( dev );
        return 0;
    }

    int i = fgetc(dev);
    assert( i == ETX );

    return length_read;
}


static size_t vread( char * b, size_t length, uint16_t * crc, FILE * dev ){
    r2_debug_printf( "crc before: %" PRIx16 "\n", *crc );
    // r2_debug_printf( "%zu bytes available\n", r2_bytes_available_fp( dev ) );
    // r2_debug_printf_bytes_available_fp( dev );
    // TODO: adjust VMIN & VTIME here as needed for reliable behavior
    r2_debug_printf( "reading %zu bytes\n", length );
    size_t bytes_read = fread( b, 1, length, dev );
    r2_debug_printf( "read %zu bytes\n", bytes_read );
    if( bytes_read != length ) {
        fprintf( stderr, "tried to read %zu bytes, but only got %zu\n",
                length, bytes_read );
    }
    if( bytes_read > 0 ) {
        *crc = crc16xmodem_byte( *crc, b, bytes_read );
    }
    r2_debug_printf( "crc after: %" PRIx16 "\n", *crc );
    return bytes_read;
}


static size_t vreadu16( uint16_t * u, uint16_t * crc, FILE * dev ) {
    size_t n = vread( (char *)u, 2, crc, dev );
    *u = ntohs( *u );
    r2_debug_printf( "vreadi32: %" PRIu32 "\n", *u );
    return n;
}


static size_t vreadi32( int32_t * i, uint16_t * crc, FILE * dev ) {
    size_t n = vread( (char *)i, 4, crc, dev );
    *i = (int32_t)ntohl( (uint32_t)*i );
    r2_debug_printf( "vreadi32: %" PRId32 "\n", *i );
    return n;
}


static size_t vreadf16( float * f, const float scale, uint16_t * crc,
        FILE * dev ) {
    int16_t ni = 0;
    size_t n = vread( (char *)&ni, 2, crc, dev );
    r2_debug_printf( "vreadf16, ni: %" PRId16 " 0x04%" PRIx16 "\n", ni, ni );
    *f = ni16tohf( ni, scale );
    r2_debug_printf( "vreadf16: %f\n", *f );
    return n;
}


static size_t vreadf32( float * f, const float scale, uint16_t * crc,
        FILE * dev ) {
    int32_t ni = 0;
    size_t n = vread( (char *)&ni, 4, crc, dev );
    *f = ni32tohf( ni, scale );
    r2_debug_printf( "vreadf32: %f\n", *f );
    return n;
}


static size_t vread_header( FILE * dev ){
    int i = 0;
    size_t reported_length = 0;

    // TODO: Refactor the logic below into a cleaner switch statement.
    while( reported_length == 0 && ( ( i = fgetc( dev ) ) != EOF ) ) {
        if ( ferror( dev ) != 0 ) {
            perror( "vread_header() fgetc()" );
            return 0;
        } else if( (char)i == STX ) { // found a short VESC packet
            if( ( reported_length = fgetc( dev ) ) != EOF ) {
                r2_debug_printf( "found STX (0x%02hhX), length = %zu\n",
                        (char)i, reported_length ); // TODO: do I really need the char cast? how does LTIB for LPC3250 behave without it?
                return reported_length;
            } else {
                fputs( "vread_header, fgetc=EOF on packet length\n", stderr );
                return 0;
            }
        } else if ( (char)i == ETX ) { // found a long VESC packet
            uint16_t rl_uint16 = 0;
            if( 2 == vreadu16( &rl_uint16, NULL, dev ) ) {
                reported_length = rl_uint16;
                r2_debug_printf( "found ETX (0x%02hhX), length = %zu\n",
                        (char)i, reported_length );
                return reported_length;
            } else {
                fputs( "vread_header, unable to read long packet length\n",
                        stderr );
                return 0;
            }
        } else {
            fprintf( stderr, "searching for STX or ETX, found (0x%02hhX)\n",
                    (char)i );
            // TODO: Add an extra check on how many bytes are still available, even if it slows us down, it may keep us from stalling out.
        }
    }
    if( feof( dev ) ){
        fprintf( stderr, "vread_header, got EOF\n" );
        return 0;
    }
    fputs( "vread_header, not sure how I exited the loop", stderr );
    return 0;
}


int vesc_read_packet( void * payload, size_t n, FILE * dev ) {
    int packet_id = 0;
    uint16_t crc = 0;
    size_t length = vread_header( dev );
    if( 0 == length ) {
        fputs( "could not read VESC packet header\n", stderr );
        return -1;
    } else if ( n < length ) {
        fprintf( stderr, "insufficient space for payload\n" );
        return -1;
    }
    if( 1 != vread( (char *)&packet_id, 1, &crc, dev ) ) {
        fputs( "could not read VESC packet id\n", stderr );
        return -1;
    }
    switch( packet_id ) {
        case COMM_GET_VALUES:
            r2_debug_printf( "reading VESC status packet %d\n", packet_id );
            if( vread_status( payload, length, &crc, dev ) == EXIT_FAILURE ) {
                fputs( "trouble reading VESC status\n", stderr );
            }
            break;
        default:
            fprintf( stderr, "no reader implemented for packet id %d\n",
                    packet_id );
    }
    // TODO: flush extra input here if the reader failed

    uint16_t crc_calculated = crc;
    r2_debug_printf( "calculated CRC = %04" PRIx16 "\t", crc );
    uint16_t crc_reported = 0;
    if( 2 != vreadu16( &crc_reported, &crc, dev ) ) {
        fputs( "could not read crc from packet\n", stderr );
        return -1;
    }
    r2_debug_printf( "reported CRC = %04" PRIx16 "\t", crc_reported );
    r2_debug_printf( "inclusive CRC = %04" PRIx16 "\t", crc );

    // finally, check the inclusive CRC
    if( 0 != crc ) {
        fprintf( stderr, "warning: inclusive CRC = %04" PRIx16 "\t", crc );
        fprintf( stderr, "calculated %04" PRIx16 " != %04" PRIx16 " reported\n",
                crc_calculated, crc_reported );
        return -1;
    }
    int final_byte = 0;
    if( ( final_byte = fgetc( dev ) ) == EOF ) {
        fputs( "got EOF instead of last byte\n", stderr );
    }
    assert( ETX == final_byte );
    return packet_id;
}


static size_t vread_fault( lvesc_status * s, uint16_t * crc, FILE * dev ){
    size_t n = vread( (char *)&(s->fault), 1, crc, dev );
    r2_debug_printf( "VESC fault byte 0x%02x\n", s->fault );
    return n;
}


static int vread_status( lvesc_status * s, size_t n, uint16_t * crc,
        FILE * dev ) {
    if( n != VESC_STATUS_PAYLOAD_LENGTH ) {
        fprintf( stderr, "packet says it has %zu bytes, expected %d\n",
                n, VESC_STATUS_PAYLOAD_LENGTH );
        return EXIT_FAILURE;
    }
    // TODO: instead of returning EXIT_SUCCESS | EXIT_FAILURE, decrement n as you read, and return it, so that the caller gets an idea of how many bytes should be left to flush
    if( 2 != vreadf16( &(s->mosfet_temperature), 1e1, crc, dev ) ) {
        fprintf( stderr, "could not read mosfet temperature\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "mosfet_temperature: %f\n", s->mosfet_temperature );
    if( 2 != vreadf16( &(s->motor_temperature), 1e1, crc, dev ) ) {
        fprintf( stderr, "could not read motor temperature\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "motor_temperature: %f\n", s->motor_temperature );
    if( 4 != vreadf32( &(s->motor_current), 1e2, crc, dev ) ) {
        fprintf( stderr, "could not read motor current\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "motor_current: %f\n", s->motor_current );
    if( 4 != vreadf32( &(s->input_current), 1e2, crc, dev ) ) {
        fprintf( stderr, "could not read input current\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "input_current: %f\n", s->input_current );
    if( 4 != vreadf32( &(s->id), 1e2, crc, dev ) ) {
        fprintf( stderr, "could not read id\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "id: %f\n", s->id );
    if( 4 != vreadf32( &(s->iq), 1e2, crc, dev ) ) {
        fprintf( stderr, "could not read iq\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "iq: %f\n", s->iq );
    if( 2 != vreadf16( &(s->duty_cycle), 1e3, crc, dev ) ) {
        fprintf( stderr, "could not read duty cycle\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "duty_cycle: %f\n", s->duty_cycle );
    if( 4 != vreadf32( &(s->motor_speed), 1e0, crc, dev ) ) {
        fprintf( stderr, "could not read motor speed\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "motor_speed: %f\n", s->motor_speed );
    if( 2 != vreadf16( &(s->input_voltage), 1e1, crc, dev ) ) {
        fprintf( stderr, "could not read input voltage\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "input_voltage: %f\n", s->input_voltage );
    if( 4 != vreadf32( &(s->charge), 1e4, crc, dev ) ) {
        fprintf( stderr, "could not read charge\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "charge: %f\n", s->charge );
    if( 4 != vreadf32( &(s->charge_regenerated), 1e4, crc, dev ) ) {
        fprintf( stderr, "could not read charge regenerated\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "charge_regenerated: %f\n", s->charge_regenerated );
    if( 4 != vreadf32( &(s->energy), 1e4, crc, dev ) ) {
        fprintf( stderr, "could not read energy\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "energy: %f\n", s->energy );
    if( 4 != vreadf32( &(s->energy_regenerated), 1e4, crc, dev ) ) {
        fprintf( stderr, "could not read energy_regenerated\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "energy_regenerated: %f\n", s->energy_regenerated );
    if( 4 != vreadi32( &(s->tachometer), crc, dev ) ) {
        fprintf( stderr, "could not read tachometer\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "tachometer: %" PRId32 "\n", s->tachometer );
    if( 4 != vreadi32( &(s->tachometer_absolute), crc, dev ) ) {
        fprintf( stderr, "could not read tachometer_absolute\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "tachometer_absolute: %" PRId32 "\n", s->tachometer_absolute );
    if( 1 != vread_fault( s, crc, dev ) ) {
        fprintf( stderr, "could not read VESC fault codes\n" );
        return EXIT_FAILURE;
    }
    //
    if( 4 != vreadf32( &(s->pid_position), 1e6, crc, dev ) ) {
        fprintf( stderr, "could not read pid_position\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "pid_position: %f\n", s->pid_position );
    if( 1 != vread( (char *)&(s->controller_id), 1, crc, dev ) ) {
        fprintf( stderr, "could not read controller id\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "controller_id: %" PRId8 "\n", s->controller_id );
    if( 2 != vreadf16( &(s->ntc_temp_mos[0]), 1e1, crc, dev ) ) {
        fprintf( stderr, "could not read ntc_temp_mos 1\n" );
        return EXIT_FAILURE;
    }
    if( 2 != vreadf16( &(s->ntc_temp_mos[1]), 1e1, crc, dev ) ) {
        fprintf( stderr, "could not read ntc_temp_mos 2\n" );
        return EXIT_FAILURE;
    }
    if( 2 != vreadf16( &(s->ntc_temp_mos[2]), 1e1, crc, dev ) ) {
        fprintf( stderr, "could not read ntc_temp_mos 3\n" );
        return EXIT_FAILURE;
    }
    r2_debug_printf( "ntc_temp_mos = %f,%f,%f\n", s->ntc_temp_mos[0],
            s->ntc_temp_mos[1], s->ntc_temp_mos[2] );
    return EXIT_SUCCESS;
}


void vesc_set_duty_cycle( FILE * dev, const float duty_cycle ) {
    // VESC accepts duty cycle in range [-1e5, 1e5]
    int32_t iduty = hftoni32( duty_cycle, 1e5 );
    size_t n = vesc_write( COMM_SET_DUTY, &iduty, 4, dev );
    assert( n == 10 );
}


void vesc_set_current( FILE * dev, const float current ) {
    int32_t icurrent = hftoni32( current, 1e5 ); // TODO: check scale
    // TODO: does the message have a direction parameter?
    size_t n = vesc_write( COMM_SET_CURRENT, &icurrent, 4, dev );
    assert( n == 10 );
}


void vesc_set_speed( FILE * dev, const float speed ) {
    int32_t ispeed = hftoni32( speed, 1e5 ); // TODO: check scale
    size_t n = vesc_write( COMM_SET_RPM, &ispeed, 4, dev );
    assert( n == 10 );
}


void vesc_fprintf_status( FILE * f, lvesc_status * status ) {
    fprintf( f, "VESC %d\tin: %2.1f V, %3.2f A\t", status->controller_id,
            status->input_voltage, status->input_current );
    fprintf( f, "out: %3.1f%%, %3.2f A, %6.1f eRPM\n",
            status->duty_cycle * 1e2, status->motor_current, status->motor_speed );
}


void vesc_fprintf_temperatures( FILE * f, lvesc_status * status ) {
    fprintf( f, "motor: %f C, MOSFET: %f C",
            status->motor_temperature, status->mosfet_temperature );
}


